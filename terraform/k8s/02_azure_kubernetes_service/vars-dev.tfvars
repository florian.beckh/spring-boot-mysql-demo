tenant_id = "a955d80e-ee47-4965-bac2-6ae82643cb48"
subscription_id = "50dd727e-dd82-4889-8ab8-0ec86b7ffea7"

app_name = "workaks"
dns_prefix = "workakscc"
environment = "dev"

location = {
  name  = "westeurope"
  short = "weu"
}

default_node_pool = {
  name = "default"
  node_count = 2
  vm_size = "Standard_B8ms"
}

additional_node_pool = {
  name = "additional"
  node_count = 2
  vm_size = "Standard_B8ms"
}

tags = {
  "Team" = "workshop"
  "Deparment" = "development"
}

