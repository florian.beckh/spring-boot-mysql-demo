variable "subscription_id" {
  type = string
}

variable "location" {
  type = string
}

variable "tenant_id" {
  type = string
}

variable "environment" {
  type = string
}

variable "app_name" {
  type = string
}

variable "key_vault" {
  type = object({
    name           = string
    resource_group = string
  })
}

variable "tags" {
  type = map(string)
}

variable "app_service_plan" {
  type = object({
    kind        = string
    reserved    = string
    tier        = string
    size        = string
  })
}
variable "image_repository" {
  type = string
}

variable "image_tag" {
  type = string
}

variable "image_staging_repository" {
  type = string
}

variable "image_staging_tag" {
  type = string
}

