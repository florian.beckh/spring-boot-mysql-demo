resource "azurerm_resource_group" "project" {
  name     = "${var.app_name}-${var.environment}-${var.location.short}-rg"
  location = var.location.name
  tags     = var.tags
}

