terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
  backend "azurerm" {
    tenant_id            = "a955d80e-ee47-4965-bac2-6ae82643cb48"
    subscription_id      = "50dd727e-dd82-4889-8ab8-0ec86b7ffea7"
    resource_group_name  = "inf-dev-weu-rg"
    storage_account_name = "infdevweusttf"
    container_name       = "tfstate"
    key                  = "wapp/dev/database.tfstate"
  }
}

provider "azurerm" {
  tenant_id            = "a955d80e-ee47-4965-bac2-6ae82643cb48"
  subscription_id      = "50dd727e-dd82-4889-8ab8-0ec86b7ffea7"
  features {}
}

