resource "azurerm_resource_group" "project" {
  name     = "${var.app_name}-${var.environment}-${var.location.short}-rg"
  location = var.location.name
  tags     = var.tags
}

resource "azurerm_role_assignment" "gitlab_work_principal" {
  scope                            = azurerm_resource_group.project.id
  role_definition_name             = "Contributor"
  principal_id                     = var.gitlab_work_principal_id
  skip_service_principal_aad_check = true
}
