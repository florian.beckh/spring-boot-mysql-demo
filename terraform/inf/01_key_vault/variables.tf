variable "subscription_id" {
  type = string
}

variable "location" {
  type = object({
    name = string
    short = string
  })
}

variable "tenant_id" {
  type = string
}

variable "environment" {
  type = string
}

variable "app_name" {
  type = string
}

variable "tags" {
  type = map(string)
}

