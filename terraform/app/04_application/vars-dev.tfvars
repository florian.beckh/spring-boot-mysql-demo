tenant_id = "a955d80e-ee47-4965-bac2-6ae82643cb48"
subscription_id = "50dd727e-dd82-4889-8ab8-0ec86b7ffea7"

app_name = "work"
environment = "dev"

location = "weu"

app_service_plan = {
  kind     = "linux"
  reserved = "true"
  tier     = "PremiumV2"
  size     = "P1v2"
}

image_staging_repository = "adminer"
image_staging_tag        = "4.8.0-standalone"

image_repository = "fbhcc/spring-boot-mysql-demo"
image_tag        = "1.0"

key_vault = {
  name           = "infdevweukv"
  resource_group = "inf-dev-weu-rg"
}

tags = {
  "Team" = "workshop"
  "Department" = "development"
}

