package com.example.demo.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.UUID;


@Data
public class Country {

    @Id
    private UUID id;

    private int code;

    private String alpha2;

    private String alpha3;

    private String langCs;

    private String langDe;

    private String langEn;

    private String langEs;

    private String langFr;

    private String langIt;

    private String langNl;
}
