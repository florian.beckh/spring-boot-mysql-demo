package com.example.demo.controller;

import com.example.demo.model.Country;
import com.example.demo.persistence.CountryRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@AllArgsConstructor
@RequestMapping( path = "/" )
public class CountryController {

    private final CountryRepository countryRepository;

    @GetMapping( produces = MediaType.APPLICATION_JSON_VALUE )
    public Collection<Country> getCountries() {
        return StreamSupport
               .stream(countryRepository.findAll().spliterator(), false)
               .collect(Collectors.toList());
    }
}
