data "azurerm_resource_group" "application" {
  name = "${var.app_name}-${var.environment}-${var.location}-rg"
}
