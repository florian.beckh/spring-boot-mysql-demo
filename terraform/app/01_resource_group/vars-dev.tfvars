tenant_id = "a955d80e-ee47-4965-bac2-6ae82643cb48"
subscription_id = "50dd727e-dd82-4889-8ab8-0ec86b7ffea7"

app_name = "work"
environment = "dev"

gitlab_work_principal_id = "58107ecf-2070-448f-9678-c0824d718974"

location = {
  name  = "westeurope"
  short = "weu"
}

tags = {
  "Team" = "workshop"
  "Department" = "development"
  "Department2" = "Verkauf"
}
