variable "subscription_id" {
  type = string
}

variable "location" {
  type = string
}

variable "tenant_id" {
  type = string
}

variable "environment" {
  type = string
}

variable "app_name" {
  type = string
}

variable "key_vault" {
  type = object({
    name           = string
    resource_group = string
  })
}

variable "tags" {
  type = map(string)
}


