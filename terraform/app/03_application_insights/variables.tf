variable "subscription_id" {
  type = string
}

variable "location" {
  type = string
}

variable "tenant_id" {
  type = string
}

variable "environment" {
  type = string
}

variable "app_name" {
  type = string
}

variable "application_type" {
  type = string
}

variable "retention_in_days" {
  type = number
}

variable "alert_mail_receiver" {
  type = string
}

variable "tags" {
  type = map(string)
}


