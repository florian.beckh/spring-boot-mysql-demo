tenant_id = "a955d80e-ee47-4965-bac2-6ae82643cb48"
subscription_id = "50dd727e-dd82-4889-8ab8-0ec86b7ffea7"

app_name = "work"
environment = "dev"

location = "weu"

application_type = "java"
retention_in_days = 30

alert_mail_receiver = "florian.beckh@codecentric.de"

tags = {
  "Team" = "workshop"
  "Department" = "development"
}

