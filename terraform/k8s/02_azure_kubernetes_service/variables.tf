variable "subscription_id" {
  type = string
}

variable "location" {
  type = object({
    name = string
    short = string
  })
}

variable "tenant_id" {
  type = string
}

variable "environment" {
  type = string
}

variable "app_name" {
  type = string
}

variable "dns_prefix" {
  type = string
}

variable "default_node_pool" {
  type = object({
    name       = string
    node_count = number
    vm_size    = string
  })
}

variable "additional_node_pool" {
  type = object({
    name       = string
    node_count = number
    vm_size    = string
  })
}

variable "tags" {
  type = map(string)
}

