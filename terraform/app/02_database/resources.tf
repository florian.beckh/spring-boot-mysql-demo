resource "azurerm_mysql_server" "database_server" {
  name                = "${var.app_name}-${var.environment}-${var.location}-db"
  location 						= data.azurerm_resource_group.application.location
  resource_group_name = data.azurerm_resource_group.application.name 

  administrator_login          = data.azurerm_key_vault_secret.admin_user.value
  administrator_login_password = data.azurerm_key_vault_secret.admin_pw.value

  sku_name   = "GP_Gen5_2"
  storage_mb = 5120
  version    = "8.0"

  auto_grow_enabled                 = false
  backup_retention_days             = 7
  geo_redundant_backup_enabled      = false
  infrastructure_encryption_enabled = false
  public_network_access_enabled     = true
  ssl_enforcement_enabled           = true
  ssl_minimal_tls_version_enforced  = "TLS1_2"
}

resource "azurerm_mysql_database" "database" {
  name                = "countries"
  resource_group_name = data.azurerm_resource_group.application.name 
  server_name         = azurerm_mysql_server.database_server.name
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}

resource "azurerm_mysql_configuration" "database_audit" {
  name                = "audit_log_enabled"
  resource_group_name = data.azurerm_resource_group.application.name 
  server_name         = azurerm_mysql_server.database_server.name
  value               = "ON"
}

resource "azurerm_key_vault_secret" "work_dev_weu_wapp_conn_string" {
  name         = "work-dev-weu-wapp-conn-string"
  value        = "jdbc:mysql://${azurerm_mysql_server.database_server.fqdn}/${azurerm_mysql_database.database.name}?serverTimezone=UTC&user=${data.azurerm_key_vault_secret.wapp_country_user.value}@${azurerm_mysql_server.database_server.name}&password=${data.azurerm_key_vault_secret.wapp_country_pw.value}"
  key_vault_id = data.azurerm_key_vault.key_vault.id
}

resource "null_resource" "country_db_credentials" {
  triggers = {
    db_name           = azurerm_mysql_database.database.name
    db_host           = azurerm_mysql_server.database_server.fqdn
    db_username       = "${data.azurerm_key_vault_secret.wapp_country_user.value}"
    db_password       = "${data.azurerm_key_vault_secret.wapp_country_pw.value}"
    db_admin_username = "${data.azurerm_key_vault_secret.admin_user.value}@${azurerm_mysql_server.database_server.name}"
    db_admin_password = "${data.azurerm_key_vault_secret.admin_pw.value}"
  }

  provisioner "local-exec" {
    command = <<EOT
      mysql -u "${self.triggers.db_admin_username}" -p"${self.triggers.db_admin_password}" -h "${self.triggers.db_host}" -D "${self.triggers.db_name}" -e "CREATE USER '${self.triggers.db_username}'@'%' IDENTIFIED BY '${self.triggers.db_password}'" 
      mysql -u "${self.triggers.db_admin_username}" -p"${self.triggers.db_admin_password}" -h "${self.triggers.db_host}" -D "${self.triggers.db_name}" -e "GRANT ALL PRIVILEGES ON countries . * TO  '${self.triggers.db_username}'@'%'" 
    EOT
  }
}

resource "azurerm_mysql_firewall_rule" "database_server_local_access" {
  name                = "fbhcc"
  resource_group_name = data.azurerm_resource_group.application.name
  server_name         = azurerm_mysql_server.database_server.name
  start_ip_address    = "188.194.239.157"
  end_ip_address      = "188.194.239.157"
}

resource "azurerm_mysql_firewall_rule" "database_server_azure_cli_access" {
  name                = "azure_cli"
  resource_group_name = data.azurerm_resource_group.application.name
  server_name         = azurerm_mysql_server.database_server.name
  start_ip_address    = "20.86.157.203"
  end_ip_address      = "20.86.157.203"
}

# allow access to azure resoruces https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/mysql_firewall_rule
resource "azurerm_mysql_firewall_rule" "azure_services" {
  name                = "azure_services"
  resource_group_name = data.azurerm_resource_group.application.name
  server_name         = azurerm_mysql_server.database_server.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "0.0.0.0"
}
