resource "azurerm_container_registry" "acr" {
  name                     = var.acr_name 
  resource_group_name      = data.azurerm_resource_group.keyvault.name
  location                 = var.location.name
  sku                      = "Basic"
  admin_enabled            = true
}
