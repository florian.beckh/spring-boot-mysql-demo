CREATE TABLE country
(
    id                         varchar(36)  NOT NULL,
    code                       INT          NOT NULL,
    alpha2                     VARCHAR(255) NOT NULL,
    alpha3                     VARCHAR(255) NOT NULL,
    lang_cs                    VARCHAR(255) NOT NULL,
    lang_de                    VARCHAR(255) NOT NULL,
    lang_en                    VARCHAR(255) NOT NULL,
    lang_es                    VARCHAR(255) NOT NULL,
    lang_fr                    VARCHAR(255) NOT NULL,
    lang_it                    VARCHAR(255) NOT NULL,
    lang_nl                    VARCHAR(255) NOT NULL,

    PRIMARY KEY (id),

    CONSTRAINT UK_country_code UNIQUE (code),
    CONSTRAINT UK_country_alpha2 UNIQUE (alpha2),
    CONSTRAINT UK_country_alpha3 UNIQUE (alpha3),

    CONSTRAINT CHK_country_code CHECK (code > 0 AND code <= 999)
);

