data "azurerm_resource_group" "application" {
  name = "${var.app_name}-${var.environment}-${var.location}-rg"
}

data "azurerm_key_vault" "key_vault" {
  name                = var.key_vault.name
  resource_group_name = var.key_vault.resource_group
}

data "azurerm_key_vault_secret" "conn_string" {
  key_vault_id = data.azurerm_key_vault.key_vault.id
  name = "${var.app_name}-${var.environment}-${var.location}-wapp-conn-string"
}


data "azurerm_application_insights" "application_insights" {
  name                = "${var.app_name}-${var.environment}-${var.location}-appi"
  resource_group_name = data.azurerm_resource_group.application.name
}

