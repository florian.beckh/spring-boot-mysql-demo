resource "azurerm_application_insights" "application_insights" {
  name                = "${var.app_name}-${var.environment}-${var.location}-appi"
  resource_group_name = data.azurerm_resource_group.application.name 
  location 						= data.azurerm_resource_group.application.location

  application_type    = var.application_type
  retention_in_days   = var.retention_in_days
  tags                = var.tags
}

resource "azurerm_monitor_action_group" "send_mail" {
  name                = "Send Mail - ${var.environment}"
  resource_group_name = data.azurerm_resource_group.application.name 
  short_name          = "Alert"

  email_receiver {
    name          = "Alert mailing list"
    email_address = var.alert_mail_receiver
  }

  tags = var.tags
}

resource "azurerm_monitor_scheduled_query_rules_alert" "no_requests" {
  name                = "No Requests on Country App - ${var.environment}"
  location 						= data.azurerm_resource_group.application.location
  resource_group_name = data.azurerm_resource_group.application.name 

  enabled             = true
  data_source_id      = azurerm_application_insights.application_insights.id
  description         = "Something is wrong, when no requests are made!"

  action {
    action_group      = [azurerm_monitor_action_group.send_mail.id]
    email_subject     = "Alert! No requests found"
  }

  query = <<-QUERY
   requests | count
  QUERY

  severity    = 3
  time_window = 5
  frequency   = 5

  trigger {
    operator  = "LessThan"
    threshold = 10
  }

  tags = var.tags
}

