resource "azurerm_app_service_plan" "application" {

  name                = "${var.app_name}-${var.environment}-${var.location}-aplan"
  resource_group_name = data.azurerm_resource_group.application.name 
  location 						= data.azurerm_resource_group.application.location
  reserved						= var.app_service_plan.reserved
  kind								= var.app_service_plan.kind

  sku {
    tier = var.app_service_plan.tier
    size = var.app_service_plan.size
  }

  tags                = var.tags
}

resource "azurerm_key_vault_access_policy" "wapp" {
  key_vault_id = data.azurerm_key_vault.key_vault.id
  tenant_id    = azurerm_app_service.application.identity.0.tenant_id
  object_id    = azurerm_app_service.application.identity.0.principal_id

  key_permissions = [
    "Get",
  ]

  secret_permissions = [
    "Get",
  ]
}

resource "azurerm_app_service" "application" {
  name                = "${var.app_name}-${var.environment}-${var.location}-wapp"
  location            = azurerm_app_service_plan.application.location
  resource_group_name = data.azurerm_resource_group.application.name
  app_service_plan_id = azurerm_app_service_plan.application.id

  https_only          = true


  app_settings = {
    APPLICATIONINSIGHTS_INSTRUMENTATIONKEY = data.azurerm_application_insights.application_insights.instrumentation_key
    APPLICATIONINSIGHTS_CONNECTION_STRING = data.azurerm_application_insights.application_insights.connection_string

    APPINSIGHTS_PROFILERFEATURE_VERSION = "1.0.0"
    APPINSIGHTS_SNAPSHOTFEATURE_VERSION = "1.0.0"
    APPLICATIONINSIGHTS_CONFIGURATION_CONTENT = ""
    ApplicationInsightsAgent_EXTENSION_VERSION = "~3"
    DiagnosticServices_EXTENSION_VERSION = "~3"
    InstrumentationEngine_EXTENSION_VERSION = "disabled"
    SnapshotDebugger_EXTENSION_VERSION = "disabled"
    XDT_MicrosoftApplicationInsights_BaseExtensions = "disabled"
    XDT_MicrosoftApplicationInsights_Mode = "recommended"
    XDT_MicrosoftApplicationInsights_PreemptSdk = "disabled"


    WEBSITES_ENABLE_APP_SERVICE_STORAGE = false

    WEBSITES_PORT = 40000
    DOCKER_REGISTRY_SERVER_URL = "https://index.docker.io"
  }

  connection_string {
    name  = "URL"
    type  = "MySQL"
    value = "@Microsoft.KeyVault(SecretUri=${data.azurerm_key_vault_secret.conn_string.id})"
  }

  identity {
    type = "SystemAssigned"
  }

  site_config {
    linux_fx_version = "DOCKER|${var.image_repository}:${var.image_tag}"
    always_on        = true
  }

  tags                = var.tags
}

resource "azurerm_key_vault_access_policy" "wapp_staging" {
  key_vault_id = data.azurerm_key_vault.key_vault.id
  tenant_id    = azurerm_app_service_slot.staging.identity.0.tenant_id
  object_id    = azurerm_app_service_slot.staging.identity.0.principal_id

  key_permissions = [
    "Get",
  ]

  secret_permissions = [
    "Get",
  ]
}

resource "azurerm_app_service_slot" "staging" {
  name                = "staging"
  app_service_name    = azurerm_app_service.application.name
  location            = data.azurerm_resource_group.application.location
  resource_group_name = data.azurerm_resource_group.application.name
  app_service_plan_id = azurerm_app_service_plan.application.id

  identity {
    type = "SystemAssigned"
  }

  site_config {
    linux_fx_version = "DOCKER|${var.image_staging_repository}:${var.image_staging_tag}"
    always_on        = true
  }

  app_settings = {
    WEBSITES_ENABLE_APP_SERVICE_STORAGE = false
    DOCKER_REGISTRY_SERVER_URL = "https://index.docker.io"
  }

  tags = var.tags
}

