locals {
  swap_source = "staging" 
  swap_target = "${var.app_name}-${var.environment}-${var.location}-wapp"
}

resource "azurerm_app_service_active_slot" "prod_swap" {
  resource_group_name   = data.azurerm_resource_group.application.name
  app_service_name      = local.swap_target 
  app_service_slot_name = local.swap_source
}
