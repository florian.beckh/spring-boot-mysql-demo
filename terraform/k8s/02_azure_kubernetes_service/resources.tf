resource "azurerm_kubernetes_cluster" "cluster" {
  name                = "${var.app_name}-${var.environment}-${var.location.short}-aks"
  resource_group_name = data.azurerm_resource_group.keyvault.name
  location            = var.location.name
  dns_prefix          = var.dns_prefix

  default_node_pool {
    name                  = var.default_node_pool.name
    vm_size               = var.default_node_pool.vm_size
    node_count            = var.default_node_pool.node_count
  }

  identity {
    type = "SystemAssigned"
  }

  tags = var.tags
}


resource "azurerm_kubernetes_cluster_node_pool" "additional_node_pool" {
  name                  = var.additional_node_pool.name
  vm_size               = var.additional_node_pool.vm_size
  node_count            = var.additional_node_pool.node_count
  kubernetes_cluster_id = azurerm_kubernetes_cluster.cluster.id 

  tags = {
    Environment = "Production"
  }
}
