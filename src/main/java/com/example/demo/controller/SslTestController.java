package com.example.demo.controller;

import lombok.AllArgsConstructor;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import java.security.GeneralSecurityException;
import java.security.KeyStore;

@RestController
public class SslTestController {

    @Value("${server.port}")
    private int applicationPort;

    @GetMapping(value = "/ssl-test")
    public String inbound(){
        return "Inbound TLS is working!!";
    }

    @GetMapping(value = "/ssl-test-outbound")
    public String outbound() throws GeneralSecurityException {
        var ks = KeyStore.getInstance(KeyStore.getDefaultType());
        var sslContext = SSLContexts.custom()
                .loadTrustMaterial(ks, new TrustSelfSignedStrategy())
                .build();

        HostnameVerifier allowAll = (String hostName, SSLSession session) -> true;
        var csf = new SSLConnectionSocketFactory(sslContext, allowAll);

        var httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();

        var requestFactory =
                new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);
        var restTemplate = new RestTemplate(requestFactory);
        var sslTest = String.format("https://localhost:%s/ssl-test", applicationPort);

        var response
                = restTemplate.getForEntity(sslTest, String.class);

        return "Outbound TLS " +
                (response.getStatusCode() == HttpStatus.OK ? "is" : "is not")  + " Working!!";
    }
}
