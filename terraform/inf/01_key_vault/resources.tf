resource "azurerm_key_vault" "environment_key_vault" {

  name                        = "${var.app_name}${var.environment}${var.location.short}kv"
  resource_group_name         = data.azurerm_resource_group.keyvault.name
  location                    = var.location.name
  sku_name                    = "standard"
  tenant_id                   = var.tenant_id
  enabled_for_disk_encryption = true
  purge_protection_enabled    = false

  tags = var.tags
}

