data "azurerm_resource_group" "application" {
  name = "${var.app_name}-${var.environment}-${var.location}-rg"
}

data "azurerm_key_vault" "key_vault" {
  name = var.key_vault.name
  resource_group_name = var.key_vault.resource_group
}

data "azurerm_key_vault_secret" "admin_user" {
  key_vault_id = data.azurerm_key_vault.key_vault.id
  name = "${var.app_name}-${var.environment}-${var.location}-db-admin-user"
}

data "azurerm_key_vault_secret" "admin_pw" {
  key_vault_id = data.azurerm_key_vault.key_vault.id
  name = "${var.app_name}-${var.environment}-${var.location}-db-admin-pw"
}

data "azurerm_key_vault_secret" "wapp_country_user" {
  key_vault_id = data.azurerm_key_vault.key_vault.id
  name = "${var.app_name}-${var.environment}-${var.location}-db-wapp-country-user"
}

data "azurerm_key_vault_secret" "wapp_country_pw" {
  key_vault_id = data.azurerm_key_vault.key_vault.id
  name = "${var.app_name}-${var.environment}-${var.location}-db-wapp-country-pw"
}

